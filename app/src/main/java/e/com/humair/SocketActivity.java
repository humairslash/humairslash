package e.com.humair;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SocketActivity extends AppCompatActivity {

    private Socket socket;
    Button btn;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthCredential credential;
    private FirebaseApp user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_socket);
        btn = findViewById(R.id.maps_btn_msg);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        final EditText phonenumber = findViewById(R.id.et_phone_number);
        Button btn_send_msg = findViewById(R.id.btn_snd_msg);

        btn_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithoutCredential(phonenumber.getText().toString());//sendText(phonenumber.getText().toString());
            }
        });
    }

    private void verifyCode(String code) {
        credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(this,new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Toast.makeText(getApplicationContext(),"Sign In Succes",Toast.LENGTH_SHORT ).show();
                    // ...
                } else {
                    // Sign in failed, display a message and update the UI
                    Toast.makeText(getApplicationContext(),"Sign In Failure",Toast.LENGTH_SHORT ).show();;
                }
            }
        });
    }

    private void sendText(String phoneNumber) {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks(){

            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                Log.e("Yay", "onVerificationCompleted:");
                Toast.makeText(getApplicationContext(),"onVerificationCompleted",Toast.LENGTH_SHORT ).show();
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_SHORT ).show();
            }

        };
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    public void sendMessage()
    {
        try {
            socket = IO.socket("http://51.89.173.64:6666");
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {

                /*    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btn.setText("Connected");
                        }
                    });*/

                    JSONArray query = new JSONArray();
                    query.put("DriverReg");
                    JSONObject filter =new JSONObject();
                    try {
                        filter.put("office_id","XYZ");
                        filter.put("call_sign","65");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    query.put(filter);
                    socket.emit("getdata", query, new Ack() {
                        @Override
                        public void call(Object... args) {

                            Log.e("Returned",args.toString());
                            JSONObject jsnobject = null;
                            try {
                                jsnobject = new JSONObject(args[0].toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Log.e("Driver Name ",jsnobject.getString("drv_Name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                   /* socket.emit("foo", "woot", new Ack() {
                        @Override
                        public void call(final Object... args) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btn.setText(args[0].toString());
                                }
                            });
                        }
                    });*/
//s                    socket.disconnect();
                }

            }).on("message1", new Emitter.Listener() {
                //message is the keyword for communication exchanges
                @Override
                public void call(final Object... args) {

                       runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                               btn.setText(args[0].toString());
                           }
                       });
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {}

            });
            socket.connect();

        }
        catch(Exception e){

        }
    }
    public void signInWithoutCredential(String phonenumber)
    {
        String phoneNumber = phonenumber;
        String smsCode = "123456";

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseAuthSettings firebaseAuthSettings = firebaseAuth.getFirebaseAuthSettings();

// Configure faking the auto-retrieval with the whitelisted numbers.
        firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode);

        PhoneAuthProvider phoneAuthProvider = PhoneAuthProvider.getInstance();
        phoneAuthProvider.verifyPhoneNumber(
                phoneNumber,
                60L,
                TimeUnit.SECONDS,
                this, /* activity */
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential credential) {
                        // Instant verification is applied and a credential is directly returned.
                        // ...
                        Toast.makeText(getApplicationContext(),"Verified User",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onVerificationFailed(@NonNull FirebaseException e) {
                        Toast.makeText(getApplicationContext(),"Non Verified User",Toast.LENGTH_SHORT).show();
                    }

                    // ...
                });

    }
    @Override
    protected void onStart() {
        super.onStart();
        user =FirebaseApp.getInstance();
    }
}
