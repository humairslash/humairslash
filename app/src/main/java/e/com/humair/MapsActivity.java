package e.com.humair;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{
    private Button btn_getloc;
    private GoogleMap mMap;
    private GPSTracker gps;
    private double lati,longi;
    private LatLng myloc;
    private PolylineOptions polyLineOptions;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        AutocompleteSupportFragment autocompleteSupportFragment =(AutocompleteSupportFragment)getSupportFragmentManager().findFragmentById(R.id.frag_autocom);
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID,Place.Field.NAME,Place.Field.LAT_LNG));
        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Toast.makeText(getApplicationContext(),"Place: " + place.getName(),Toast.LENGTH_SHORT).show();
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title("Destination")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                polyLineOptions = new PolylineOptions();
                DecimalFormat df = new DecimalFormat("##.#######");
                df.setRoundingMode(RoundingMode.DOWN);
                networkCall(String.valueOf(df.format(myloc.latitude)),String.valueOf(df.format(myloc.longitude)),String.valueOf(df.format(place.getLatLng().latitude)),String.valueOf(df.format(place.getLatLng().longitude)));
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                //  Log.i(TAG, "An error occurred: " + status);
            }
        });
        btn_getloc=findViewById(R.id.maps_btn_loc);

        btn_getloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent intent = new Intent(getApplicationContext(),SocketActivity.class);
                startActivity(intent);
            }
        });//mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.

        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        gps = new GPSTracker(this);
        setLocation();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng).title("Destination")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

               polyLineOptions = new PolylineOptions();
                DecimalFormat df = new DecimalFormat("##.#######");
                df.setRoundingMode(RoundingMode.DOWN);
                networkCall(String.valueOf(df.format(myloc.latitude)),String.valueOf(df.format(myloc.longitude)),String.valueOf(df.format(latLng.latitude)),String.valueOf(df.format(latLng.longitude)));
            }
        });
        //mMap.getUiSettings().setMyLocationButtonEnabled(true);
            //  Toast.makeText(mContext,"You need have granted permission",Toast.LENGTH_SHORT).show();

    }


    public void setLocation()
    {

        lati = gps.getLatitude();
        longi = gps.getLongitude();
        myloc = new LatLng(lati, longi);

        mMap.addMarker(new MarkerOptions().position(myloc).title("Marker At your Lcoation")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                myloc, 17.0f));

    }
    public class getData extends AsyncTask<List<LatLng>,List<LatLng>,List<LatLng>>{
         List<LatLng> myList;


        @Override
        protected List<LatLng> doInBackground(List<LatLng>... lists) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            InputStream iStream = null;
            try {
                Response response = client.newCall(request).execute();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    // do something wih the result
                    Log.e("Network Result", response.body().toString());
                    iStream= response.body().byteStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
                    StringBuffer sb = new StringBuffer();
                    String line = "";
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    String data = sb.toString();
                    Gson gson = new Gson();
                    String loc = response.message();
                    String loc2 = response.body().toString();
                    RootObject user = gson.fromJson(data, RootObject.class);
                    myList = new ArrayList<>();
                    String encoded_polyline = null;
                    for (Route point :
                            user.getRoutes()) {
                        encoded_polyline=point.getOverview_polyline().getPoints();

                    }
                    myList= PolyUtil.decode(encoded_polyline);
                    polyLineOptions.addAll(myList);
                    polyLineOptions.width(5);
                    polyLineOptions.color(Color.RED);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                try {
                    iStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return myList;
        }

            @Override
        protected void onPostExecute(List<LatLng> oid) {
            super.onPostExecute(oid);

            com.google.android.gms.maps.model.Polyline polyline = mMap.addPolyline(polyLineOptions);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(oid.get(0));
            builder.include(oid.get(oid.size() - 1));
            LatLngBounds bounds = builder.build();
            CameraPosition googlePlex = CameraPosition.builder()
                        .target(oid.get(oid.size() - 1))
                        .zoom(17)
                        .bearing(0)

                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 500, null);
            //mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));
        }
    }
    public void networkCall(String lat1,String lng1,String lat2,String lng2)
    {
        //url="https://roads.googleapis.com/v1/nearestRoads?points="+lat1+","+lng1+"|"+lat2+","+lng2+"&key=AIzaSyD7egVmMPCUsSETSpx-jgsUIV29YzZUrjc";
        url="https://maps.googleapis.com/maps/api/directions/json?origin="+lat1+","+lng1+"&destination="+lat2+","+lng2+"&key=AIzaSyD7egVmMPCUsSETSpx-jgsUIV29YzZUrjc";
        new getData().execute();

    }


    public class Location
    {
        public double latitude;
        public double longitude;

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }

    public class GeocodedWaypoint
    {
        private String geocoder_status ;
        private String place_id ;
        private List<String> types ;

        public String getGeocoder_status() {
            return geocoder_status;
        }

        public void setGeocoder_status(String geocoder_status) {
            this.geocoder_status = geocoder_status;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }
    }

    public class Northeast
    {
        private double lat ;
        private double lng ;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public class Southwest
    {
        private double lat ;
        private double lng ;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public class Bounds
    {
        private Northeast northeast ;
        private Southwest southwest ;

        public Northeast getNortheast() {
            return northeast;
        }

        public void setNortheast(Northeast northeast) {
            this.northeast = northeast;
        }

        public Southwest getSouthwest() {
            return southwest;
        }

        public void setSouthwest(Southwest southwest) {
            this.southwest = southwest;
        }
    }

    public class Distance
    {
        private String text ;
        private int value ;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public class Duration
    {
        private String text ;
        private int value ;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public class EndLocation
    {
        private double lat ;
        private double lng ;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public class StartLocation
    {
        private double lat ;
        private double lng ;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public class Distance2
    {
        private String text ;
        private int value ;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public class Duration2 {
        private String text;
        private int value;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
        public class EndLocation2 {
            private double lat;
            private double lng;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }
        }

        public class Polyline {
            private String points;

            public String getPoints() {
                return points;
            }

            public void setPoints(String points) {
                this.points = points;
            }
        }

        public class StartLocation2 {
            private double lat;
            private double lng;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }
        }

        public class Step {
            private Distance2 distance;
            private Duration2 duration;
            private EndLocation2 end_location;
            private String html_instructions;
            private Polyline polyline;
            private StartLocation2 start_location;
            private String travel_mode;
            private String maneuver;

            public Distance2 getDistance() {
                return distance;
            }

            public void setDistance(Distance2 distance) {
                this.distance = distance;
            }

            public Duration2 getDuration() {
                return duration;
            }

            public void setDuration(Duration2 duration) {
                this.duration = duration;
            }

            public EndLocation2 getEnd_location() {
                return end_location;
            }

            public void setEnd_location(EndLocation2 end_location) {
                this.end_location = end_location;
            }

            public String getHtml_instructions() {
                return html_instructions;
            }

            public void setHtml_instructions(String html_instructions) {
                this.html_instructions = html_instructions;
            }

            public Polyline getPolyline() {
                return polyline;
            }

            public void setPolyline(Polyline polyline) {
                this.polyline = polyline;
            }

            public StartLocation2 getStart_location() {
                return start_location;
            }

            public void setStart_location(StartLocation2 start_location) {
                this.start_location = start_location;
            }

            public String getTravel_mode() {
                return travel_mode;
            }

            public void setTravel_mode(String travel_mode) {
                this.travel_mode = travel_mode;
            }

            public String getManeuver() {
                return maneuver;
            }

            public void setManeuver(String maneuver) {
                this.maneuver = maneuver;
            }
        }

        public class Leg {
            private Distance distance;
            private Duration duration;
            private String end_address;
            private EndLocation end_location;
            private String start_address;
            private StartLocation start_location;
            private List<Step> steps;
            private List<Object> traffic_speed_entry;
            private List<Object> via_waypoint;

            public Distance getDistance() {
                return distance;
            }

            public void setDistance(Distance distance) {
                this.distance = distance;
            }

            public Duration getDuration() {
                return duration;
            }

            public void setDuration(Duration duration) {
                this.duration = duration;
            }

            public String getEnd_address() {
                return end_address;
            }

            public void setEnd_address(String end_address) {
                this.end_address = end_address;
            }

            public EndLocation getEnd_location() {
                return end_location;
            }

            public void setEnd_location(EndLocation end_location) {
                this.end_location = end_location;
            }

            public String getStart_address() {
                return start_address;
            }

            public void setStart_address(String start_address) {
                this.start_address = start_address;
            }

            public StartLocation getStart_location() {
                return start_location;
            }

            public void setStart_location(StartLocation start_location) {
                this.start_location = start_location;
            }

            public List<Step> getSteps() {
                return steps;
            }

            public void setSteps(List<Step> steps) {
                this.steps = steps;
            }

            public List<Object> getTraffic_speed_entry() {
                return traffic_speed_entry;
            }

            public void setTraffic_speed_entry(List<Object> traffic_speed_entry) {
                this.traffic_speed_entry = traffic_speed_entry;
            }

            public List<Object> getVia_waypoint() {
                return via_waypoint;
            }

            public void setVia_waypoint(List<Object> via_waypoint) {
                this.via_waypoint = via_waypoint;
            }
        }

        public class OverviewPolyline {
            private String points;

            public String getPoints() {
                return points;
            }

            public void setPoints(String points) {
                this.points = points;
            }
        }

        public class Route {
            private Bounds bounds;
            private String copyrights;
            private List<Leg> legs;
            private OverviewPolyline overview_polyline;
            private String summary;
            private List<Object> warnings;
            private List<Object> waypoint_order;

            public Bounds getBounds() {
                return bounds;
            }

            public void setBounds(Bounds bounds) {
                this.bounds = bounds;
            }

            public String getCopyrights() {
                return copyrights;
            }

            public void setCopyrights(String copyrights) {
                this.copyrights = copyrights;
            }

            public List<Leg> getLegs() {
                return legs;
            }

            public void setLegs(List<Leg> legs) {
                this.legs = legs;
            }

            public OverviewPolyline getOverview_polyline() {
                return overview_polyline;
            }

            public void setOverview_polyline(OverviewPolyline overview_polyline) {
                this.overview_polyline = overview_polyline;
            }

            public String getSummary() {
                return summary;
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public List<Object> getWarnings() {
                return warnings;
            }

            public void setWarnings(List<Object> warnings) {
                this.warnings = warnings;
            }

            public List<Object> getWaypoint_order() {
                return waypoint_order;
            }

            public void setWaypoint_order(List<Object> waypoint_order) {
                this.waypoint_order = waypoint_order;
            }
        }

        public class RootObject {
            private List<GeocodedWaypoint> geocoded_waypoints;
            private List<Route> routes;
            private String status;

            public List<GeocodedWaypoint> getGeocoded_waypoints() {
                return geocoded_waypoints;
            }

            public void setGeocoded_waypoints(List<GeocodedWaypoint> geocoded_waypoints) {
                this.geocoded_waypoints = geocoded_waypoints;
            }

            public List<Route> getRoutes() {
                return routes;
            }

            public void setRoutes(List<Route> routes) {
                this.routes = routes;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Places.initialize(getApplicationContext(), "AIzaSyAxhcpeZ3jkvZH7X_SiwYocRybdXP9A_oQ", Locale.US);
    }
}
